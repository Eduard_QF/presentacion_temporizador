import 'package:flutter/material.dart';
import 'package:flutter_material_color_picker/flutter_material_color_picker.dart';
import 'package:temporizador_presentaciones/model/timer.dart';
import 'package:temporizador_presentaciones/utils/utils_color.dart';
import 'package:temporizador_presentaciones/utils/utils_windows.dart';
class ColorPicker extends StatefulWidget {
  ColorPicker({Key key, this.timer}) : super(key: key);
  Timer timer;
  @override
  _ColorPickerState createState() => _ColorPickerState(timer);
}

class _ColorPickerState extends State<ColorPicker> {
    _ColorPickerState(this._timer);
    Timer _timer;

  @override
  Widget build(BuildContext context) {
    return Container(
       child:  GestureDetector(
      child: Container(
        width: 20.0,
        height: 20.0,
        decoration: BoxDecoration(color: _timer.color, shape: BoxShape.circle),
      ),
      onTap: () {
        setState(() {
          return showDialog<void>(
              context: context,
              barrierDismissible: false, // user must tap button!
              builder: (BuildContext context) {
                return DialogSelectColos();
              });
        });
      },
    ),
    );
  }

  Widget DialogSelectColos() {
    return AlertDialog(
      title: Text('Seleccione Color'),
      content: SingleChildScrollView(
        child: ListBody(
          children: <Widget>[
            CircleColorV2()  
          ],
        ),
      ),
      actions: <Widget>[
        FlatButton(
              child: Text('Cancelar'),
              onPressed: Navigator.of(context).pop,
            ),
            FlatButton(
              child: Text('Aceptar'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
      ],
    );
  }

  // ignore: non_constant_identifier_names
  Widget CircleColorV2() {
    return Container(
      width: UtilsWindows.getWidth(context),
      height: UtilsWindows.getHeight(context)/4,
      child: MaterialColorPicker(
        allowShades: false,
        selectedColor: _timer.color,
        onMainColorChange: (ColorSwatch color) {
          setState(() {
            _timer.color = color;
          });
        },
        colors: [
      Colors.red,
      Colors.deepOrange,
      Colors.yellow,
      Colors.lightGreen,
      Colors.amber,
      Colors.cyan,
      Colors.deepPurple,
      Colors.deepPurpleAccent,
      Colors.green,
      Colors.greenAccent,
      Colors.indigo,
      Colors.indigoAccent,
      Colors.lightBlue,
      Colors.lightGreen,
      Colors.lime,
      Colors.orange,
      Colors.pink,
      Colors.pinkAccent,
      Colors.pinkAccent,
      Colors.teal,
      Colors.tealAccent,
      Colors.yellowAccent,
      Colors.blue,
      Colors.blueAccent,
      ]
      ),
    );
  }
}