import 'package:flutter/material.dart';
import 'package:temporizador_presentaciones/components/color_picker.dart';
import 'package:temporizador_presentaciones/model/timer.dart';
import 'package:temporizador_presentaciones/utils/utils_windows.dart';

// ignore: must_be_immutable
class TimerCard extends StatefulWidget {
  TimerCard({Key key, this.timer}) : super(key: key);
  Timer timer;

  @override
  _TimerCardState createState() => _TimerCardState(timer);
}

class _TimerCardState extends State<TimerCard> {
  _TimerCardState(this._timer);
  Timer _timer;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Card(
        child: Row(
          children: [TitleText(), TimerText(), ColorPicker(timer: _timer), ActiveCheck(), DeleteCard()],
        ),
      ),
    );
  }

  // ignore: non_constant_identifier_names
  Widget TitleText() {
    return Container(
      padding: EdgeInsets.only(left: 15.0),
      width: (UtilsWindows.getHeight(context) / 5),
      child: Text(
        _timer.name,
        style: TextStyle(
          color: Colors.black,
        ),
      ),
    );
  }

  // ignore: non_constant_identifier_names
  Widget ActiveCheck() {
    return Switch(
        value: _timer.active,
        onChanged: (value) {
          setState(() {
            _timer.active = value;
          });
        });
  }

  // ignore: non_constant_identifier_names
  Widget DeleteCard(){
    return Container(
      margin: EdgeInsets.only(left: 10.0),
      child: IconButton(
      icon: Icon(Icons.delete),
      iconSize: 25.0,
      )
    );
  }

  // ignore: non_constant_identifier_names
  Widget TimerText(){
    return Container(
      width: (UtilsWindows.getHeight(context) / 7),
      child: Text(
        getTime()
      ),
    );
  }

  String getTime(){
    String timefinal= "";
    int hours = _timer.time.inHours;
    int minuts = _timer.time.inMinutes - ( hours * 60 );
    int seconds = _timer.time.inSeconds - ( minuts * 60 ) - (hours * 60 * 60);
    return "$hours:$minuts:$seconds";
  }
}


