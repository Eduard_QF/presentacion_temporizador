import 'package:flutter/material.dart';
import 'package:temporizador_presentaciones/components/timer_card.dart';
import 'package:temporizador_presentaciones/model/compositer.dart';
import 'package:temporizador_presentaciones/views/menu_view.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(

        primarySwatch: Colors.blue,

        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MenuView(),
    );
  }
}

