import 'dart:core';

import 'package:flutter/material.dart';
import 'package:temporizador_presentaciones/model/timer.dart';

class Compositer {
  // ignore: non_constant_identifier_names
  static final Compositer _compositer_singleton = Compositer._internal();
  final List<Timer> timers;

  Compositer._internal({this.timers});
  
  factory Compositer(){
    return _compositer_singleton;
  }


  static List<Timer> getTimerDummy(){
    return [
      Timer(name: "timer 1",description: "dummy 1",color: Colors.blue, active: true,time: Duration(minutes:3,hours: 2,seconds: 45)),
      Timer(name: "timer 2",description: "dummy 2",color: Colors.green, active: true,time: Duration(minutes:2)),
      Timer(name: "timer 3",description: "dummy 3",color: Colors.red, active: true,time: Duration(minutes:5)),
      Timer(name: "timer 4",description: "dummy 4",color: Colors.cyan, active: true,time: Duration(minutes:7))
    ];
  }

} 