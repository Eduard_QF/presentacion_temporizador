import 'package:flutter/cupertino.dart';

class Timer {
  Duration   time;
  String name, description;
  Color color;
  bool active;

  Timer({this.time, 
        this.name,
        this.description, 
        this.color,
        this.active});
  
}