import 'dart:ui';

import 'package:flutter/material.dart';

class UtilsColors {
  static getColorList() {
    return [
      Colors.red,
      Colors.deepOrange,
      Colors.yellow,
      Colors.lightGreen,
      Colors.accents,
      Colors.amber,
      Colors.cyan,
    ];
  }
}
