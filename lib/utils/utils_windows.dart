import 'package:flutter/cupertino.dart';

class UtilsWindows {
  
  static getWidth(context){
     return MediaQuery.of(context).size.width;
  }

  static getHeight(context){
    return MediaQuery.of(context).size.height;
  }

}