import 'package:flutter/material.dart';
import 'package:temporizador_presentaciones/components/timer_card.dart';
import 'package:temporizador_presentaciones/model/compositer.dart';

class MenuView extends StatefulWidget {
  MenuView({Key key}) : super(key: key);

  @override
  _MenuViewState createState() => _MenuViewState();
}

class _MenuViewState extends State<MenuView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Temporizadores"),
      ),
      body:Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TimerCard(timer:Compositer.getTimerDummy()[0]),
            TimerCard(timer:Compositer.getTimerDummy()[1]),
            TimerCard(timer:Compositer.getTimerDummy()[2]),
            TimerCard(timer:Compositer.getTimerDummy()[3]),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }

  void _incrementCounter() {
    setState(() {
    });
  }
}